# CHANGELOG

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/).
  
## [6.0.1] - 2024-03-07  
  
### Fixed  
  
* Actualizamos el cálculo de la versión por defecto. Resolves issue [#11](https://gitlab.com/solusoft-public/docker/sql-sidecar/-/issues/11).  
  
## [6.0.0] - 2023-09-28  
  
### Changed  
  
* Actualizado método de instalación de NodeJs en Docker ya que el anterior estaba deprecado.  
  
## [5.0.2] - 2023-09-28  
  
### Fixed  
  
* Codificamos usuario y contraseña con URL encode para tratar posibles caracteres raros.  
  
## [5.0.1] - 2023-07-19  
  
### Fixed  
  
* Fix en el script que espera a que un servicio esté listo, ya que antes paraba en el primer error.  
  
## [5.0.0] - 2022-09-30  
  
### Added  
  
* Cambiada imagen base a un SQL Server 2019 con base Ubuntu 18.  
  
### Fixed  
  
* Fix en la construcción de URLs para permitir conexiones a servidores como SRVDESA7\SQL2012.  
  
## [4.0.0] - 2022-03-15  
  
### Added  
  
* Permitido el versionado de varios esquemas en la misma base de datos.  
  
## [3.5.0] - 2021-12-14  
  
### Added  
  
* Se añade la variable de entorno ENABLE_BACKUPS para activar o no la realización de backups.  
  
## [3.4.0] - 2021-07-16

### Added

* Se añade un procedimiento de soporte para verificar que los procedimientos en la base de datos compilan adecuadamente. support/install/2.sql

### Changed

* Se elimina el script que obtiene la version por defecto y se integra en el codigo Node, ya que a veces podia fallar bajo algunas circunstancias.
* Se incopora una comprobación adicional a health/ready que verifica que los procedimientos "compilan", es decir, que no hay errores que pueden ser detectador por el planificador de consultas, en cuyo caso fallará el healtcheck.
* Ahora si el resultado del healthcheck es 503, se interrumpe el script de waitFroService para evitar que se queden los runner bloqueados indefinidamente cuando el healthcheck no pasa

## [3.3.0] - 2020-06-30

### Added

* Se añade un script read_last_version.sh que crea y devuelve una variable de entorno con el SEMVER más alto de la carpeta de scripts

### Changed

* Ahora, cuando se no especifique TARGET_VERSION, se usará por defecto el valor SEMVER más alto disponible en la carpeta de scripts
    * Se añaden algunos logs para visibilizar este proceso

## [3.2.0] - 2020-10-22  
  
### Added  

* Añadida variable de entorno PREFIX_NAME para permitir modificar el prefijo de los recursos SQL de soporte.  
  
### Changed  

* Simplificados mensajes de error.  
    
## [3.1.0] - 2020-07-10  
  
### Added  

* Recuperados healthchecks  
  
### Changed  
  
* No propagamos excepción en CREATE DATABASE  
  
## [3.0.4] - 2020-05-19  
  
### Changed  
  
* Movida gestión de procesos a script Node  
* Fix de errores que evitaban compatibilidad con gitlab ci  
  
## [3.0.3] - 2020-05-18  
  
### Changed  
  
* Mejorada gestión de errores  
  
## [3.0.2] - 2020-05-12  
  
### Changed  
  
* Ejecutamos comando chmod para asegurar que se evite error de permisos  
* Añadido fichero .gitattributes  
* Modificado comando de entrypoint para parar el contenedor si no se levanta un servidor SQL  
  
## [3.0.1] - 2020-05-12  
  
### Changed  
  
* Añadido permiso de ejecución a entrypoint  
  
## [3.0.0] - 2020-05-11  
  
### Removed  

* Eliminada API  

### Changed  

* Cambiado CMD a Entrypoint en Dockerfile  
* Arreglado error en función close  
    
## [2.1.3] - 2020-02-27  
  
### Added  
  
* Añadido servidor SSH para utilizarlo en servicios de Azure  
* Añadido flujo de continuous delivery  

### Changed  

* Exportamos variable SQLCMDPASSWORD en entrypoint  

## [2.1.2] - 2020-02-27  
  
### Changed  
  
* Añadida variable de entorno PORT olvidada  
 
## [2.1.1] - 2020-02-26  
  
### Changed  
  
* Quitamos variables de entorno de la sección ENV (en esta sección no deben ir variables dinámicas)  
  
## [2.1.0] - 2020-02-14  
  
### Changed  
  
* Añadida variable de entorno para realizar actualización en el arranque del microservicio  
  
## [2.0.2] - 2020-02-13  
  
### Changed  
  
* Mejorada gestión de errores  
  
## [2.0.1] - 2020-02-12  
  
### Changed  
  
* Aumentado timeout en las request al servidor HTTP  
  
## [2.0.0] - 2020-02-12  
  
### Added  

* Renombrado endpoint del API  
  
### Changed  
  
* Arreglado error al comparar versiones semver  
* Renombrada variable SQLCMDPASSWORD a SQL_PASSWORD  
* Cambiados scripts de soporte para hacer atómicas las operaciones de lock  
  
## [1.0.3] - 2020-01-04  

### Added

* Implementado mecanismo de locks para evitar que varias instancias realicen el proceso de migración a la vez  
* Implementado proceso de backup con volúmenes antes de realizar la actualización de cada versión  

## [1.0.2] - 2020-01-02  

### Added

* Divididos healthchecks en ready y live, implementados mediante librería de godaddy  

## [1.0.1] - 2019-12-26  

### Added

* Añadido endpoint para comprobar estado  

## [1.0.0] - 2019-02-04  

### Changed

* Versión inicial  



[unreleased]: https://gitlab.com/solusoft-public/docker/sql-sidecar/compare/6.0.1...HEAD
[6.0.1]: https://gitlab.com/solusoft-public/docker/sql-sidecar/compare/6.0.0...6.0.1