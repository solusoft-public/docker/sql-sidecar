SQL Sidecar
=========================

Aplicación que controla el versionado de una base de datos SQL Server. Su propósito es servir como imagen base a un contenedor Docker, el cual mantiene un listado de scripts para cada versión, de la forma:

```
scripts/
|- 1.0.0/
|  |- 1. 03012019 - base.sql
|- 1.0.1/
|  |- 1. 03012019 - addProcedure.sql
|  |- 2. 03012019 - changeTable.sql
```

Todas las versiones deben estar en formato semver.  

La aplicación es capaz de conectarse a un servidor SQL Server ya levantado o desplegar uno en el contenedor Docker, con el propósito de realizar pruebas de integración. Actualmente no se recomienda utilizar en entornos 
de producción la base de datos en contenedor.
La aplicación realiza un backup del estado actual de la base de datos antes de actualizar a una nueva versión. Para almacenar los backups se le debe especificar al contenedor un punto de montaje para el volumen de backups.  
  
En el repositorio [SIP Database](https://gitlab.com/solusoft-idi/kubernetes-ready/sip-database) se puede observar su uso como contenedor para gestión de versiones contra una base de datos en Azure.  

## Uso  

El servicio recibe, al arranque, una variable de entorno (TARGET_VERSION) con la versión destino a la que se quiere actualizar.  
  
La aplicación va ejecutando en orden todos los scripts correspondientes a las versiones intermedias desde la versión actual, hasta la versión destino. Para la correcta ejecución de los scripts estos deberán numerarse, en caso de que se deseen mantener múltiples scripts por versión, en el orden en el que se deban aplicar. Únicamente se realizan cambios hacia delante.  

## Source code

La implementación se realiza en NodeJS.
El código fuente se encuentra en la carpeta `src`.

## Configuración

El contenedor acepta las siguientes variables de entorno:
- __START_SQL_SERVER__: "true"|"false". Indica si se arranca o no el servidor SQL Server integrado.  
- __CREATE_DATABASE__: "true"|"false". Indica si se debe crear o no la base de datos.  
- __SA_PASSWORD__: "<string>". Contraseña para el usuario con privilegios en la base de datos. Utilizado para poder crear la base de datos en caso de que se fije a "true" la variable CREATE_DATABASE.  Deberia tener mayusculas, minusculas, numeros y simbolos, conforme a los requisitos de SQL (en otro caso puede dar problemas de acceso).
- __SQL_SERVER__: "<string>". Dominio donde se encuentra el servidor SQL. Será localhost si se usa el servidor SQL integrado. Cuidado, en casos como los servidores de SRVDESA que tienen '\' en el nombre, se debe poner el nombre del servidor tal como es. Ej: SRVDESA7.intrasol.local\SQL2012.
- __SQL_USER__: "<string>". Usuario que ejecutará los scripts SQL. Se recomienda que sea "sa" si se quieren evitar problemas, aunque es posible establecer otro.
- __SQL_PASSWORD__: "<string>". Contraseña para el usuario que ejecutará los scripts SQL.  Deberia tener mayusculas, minusculas, numeros y simbolos, conforme a los requisitos de SQL (en otro caso puede dar problemas de acceso).
  - A la hora de establecer la contraseña se debe tener en cuenta que tanto por limitaciones de SQL Server como del estándar URL (a partir de los datos de conexión se construye una URL) hay símbolos que pueden dar problemas. Por lo general se pueden utilizar los siguientes caracteres sin problema:
    - Letras, mayúsculas y minúsculas.
    - Números.
    - Símbolos: -._~
  - Estas restricciones se establecen en base a la siguiente documentación:
    - [Contraseñas de SQL Server y símbolos no permitidos](https://learn.microsoft.com/en-us/previous-versions/sql/sql-server-2008/ms161962(v=sql.100)?redirectedfrom=MSDN)
    - [Símbolos no permitidos en URLs](https://www.rfc-editor.org/rfc/rfc3986#section-2.3)
- __SQL_DATABASE__: "<string>". Nombre de la base de datos.  
- __TARGET_VERSION__: "<string>". Versión destino a la que se quiere actualizar la base de datos, en formato semver.   Si no se especifica, se actualizará hasta la versión más alta disponible en la carpeta script.
- __SCRIPTS_FOLDER__: "<string>". Ruta donde se almacenan los scripts. Por defecto se almacenan en /src/scripts/.  
- __ENABLE_BACKUPS__: "true"|"false". Indica si se debe ejecutar el procedimiento de backups o no. Por defecto los backups están desactivados.  
- __BACKUPS_FOLDER__: "<string>". Ruta donde se desean almacenar los archivos de backup. Por defecto se almacenan en /src/backups/.  
- __PREFIX_NAME__: "<string>". Nombre de prefijo de las tablas y procedimientos almacenados que utiliza el sidecar como soporte. Por defecto el prefijo es "STON".  
- __DEFAULT_SCHEMA_NAME__: "<string>". Nombre del esquema de base de datos a utilizar por defecto. Solo aplica al evolucionar un sidecar de las versiones anteriores a la 4.0.0 a las versiones posteriores que sí utilizan el versionado por esquema. Su propósito es la retrocompatibilidad con dichas versiones previas, permitiendo especificar cual será el nuevo nombre de esquema a utilizar para las versiones de bases de datos ya creadas anteriormente. Para más información consultar la [Guía de migración - 4.0.0](#migración-a-400) Por defecto el nombre es "Main".  
- __SCHEMA_NAME__: "<string>". Nombre del esquema de base de datos a versionar. Por defecto el nombre es "Main".  
- __VALIDATE_SQL_CERT__: "true"|"false". Indica si se desea validad el certificado SSL al conectarse al servidor SQL.  

## Puesta en marcha

### Localmente 

Requisitos:

* NodeJS
* NPM

Instalacion:

```shell
$ cd src
$ node init.js
```

### Docker

#### Creación de la imagen  

```
docker build -t solusoft/sql-sidecar .
```

#### Opcion 1. Arranque con servidor SQL integrado  
  
```
docker run --name sql-sidecar -v <host data folder>:/var/opt/mssql/data -v <host log folder>:/var/opt/mssql/log -e 'START_SQL_SERVER=true' -e 'CREATE_DATABASE=true' -e 'SA_PASSWORD=<password>' -e 'SQL_SERVER=localhost' -e 'SQL_USER=sa' -e 'SQL_PASSWORD=<password>' -e 'SQL_DATABASE=<database>' -e 'TARGET_VERSION=<version>' -e 'VALIDATE_SQL_CERT=false' -p 1433:1433 -p 80:80 solusoft/sql-sidecar
```
  
> Nota: Si quieres que el contenedor sea __efímero__, elimina los volumenes __\<host data folder\>__ y __\<host log folder\>__ al ejecutar el comando docker run anterior.

#### Opcion 2. Arranque apuntando a un servidor SQL externo  
  
```
docker run --name sql-sidecar -e 'START_SQL_SERVER=false' -e 'CREATE_DATABASE=<create>' -e 'SQL_SERVER=<server>' -e 'SQL_USER=<user>' -e 'SQL_PASSWORD=<password>' -e 'SQL_DATABASE=<database>' -e 'TARGET_VERSION=<version>' solusoft/sql-sidecar
```
  
### Kubernetes  

El uso más común de este contenedor en Kubernetes es como contenedor "sidecar" que se ejecuta junto al contenedor principal de la aplicación y le apoya en las tareas de actualización de base de datos.
A continuación se incluye un ejemplo de este tipo de despliegue:

```yaml
containers:
  - name: sip-database-sidecar
    image: registry.gitlab.com/solusoft-idi/kubernetes-ready/sip-database:1-0-1
    resources:
      limits:
        memory: "200Mi"
        cpu: "50m"
    readinessProbe:
      httpGet:
        path: /health/ready
        port: 8080
      initialDelaySeconds: 30
      periodSeconds: 5
    livenessProbe:            
      httpGet:
        path: /health/live
        port: 8080
      initialDelaySeconds: 60
      periodSeconds: 300            
    ports:
    - containerPort: 8080
    envFrom:
    - configMapRef:
        name: database-sql-sidecar
    - secretRef:
        name: database-sql-sidecar
  - name: sip
    image: registry.gitlab.com/solusoft-idi/kubernetes-ready/sip-core
    resources:
      limits:
        memory: "200Mi"
        cpu: "50m"
    readinessProbe:
      httpGet:
        path: /health/ready
        port: 80
      initialDelaySeconds: 30
      periodSeconds: 5
    livenessProbe:            
      httpGet:
        path: /health/live
        port: 80
      initialDelaySeconds: 60
      periodSeconds: 300
    ports:
    - containerPort: 80
    envFrom:
    - configMapRef:
        name: sip        
    - secretRef:
        name: sip
  imagePullSecrets: 
    - name: docker-cfg  
```

Se deberán crear recursos ConfigMap y Secret para almacenar los valores para las variables de entorno que configuran el servicio SQL sidecar.  

## Guías de migración

### Migración a 4.0.0

En la versión 4.0.0 se ha introducido el versionado multiesquema, añadiendo una nueva columna a la tabla de versiones. Para facilitar la transición desde versiones anteriores del sidecar a la nueva versión se ha añadido una nueva variable de entorno "DEFAULT_SCHEMA_NAME" que permite definir un nombre de esquema para las versiones ya existentes en la tabla de versiones. Una vez ya se ha realizado la transición se utiliza la variable de entorno SCHEMA_NAME para definir el nombre de esquema a utilizar en el versionado. Ejemplo:

```bash
# Imaginemos que tenemos una base de datos con sidecar existente, que se ha lanzado mediante el siguiente script:
docker run --rm --name sql-sidecar -e 'START_SQL_SERVER=false' -e 'CREATE_DATABASE=false' -e 'SQL_SERVER=solusoft-sidecar-testing-server.database.windows.net' -e 'SQL_USER=soluuser' -e 'SA_PASSWORD=<password>' -e 'SQL_PASSWORD=<password>' -e 'SQL_DATABASE=solusoft-sidecar-testing' -e 'TARGET_VERSION=2.1.3' -e 'VALIDATE_SQL_CERT=false' -v ${PWD}\src\scripts:/src/scripts -v ${PWD}\src\backups:/src/backups sql-sidecar:3.5.0

# La tabla de versiones tiene este aspecto
CD_VERSION  FC_VERSION               NP_PRIM  NM_SEC  NM_REV 
1	          2022-03-14 18:33:21.910	 0	      0	      0
2	          2022-03-14 18:33:32.153	 2	      0	      0
3	          2022-03-14 18:33:34.263	 2	      1	      0
4	          2022-03-14 18:33:35.183	 2	      1	      1
5	          2022-03-14 18:33:35.813	 2	      1	      2
6	          2022-03-14 18:33:37.873	 2	      1	      3

# Mediante el siguiente script podremos actualizar a la versión 4.0.0 del sidecar y a la vez a la versión 2.1.4 de la BBDD
docker run --rm --name sql-sidecar -e 'START_SQL_SERVER=false' -e 'CREATE_DATABASE=false' -e 'SQL_SERVER=solusoft-sidecar-testing-server.database.windows.net' -e 'SQL_USER=soluuser' -e 'SA_PASSWORD=<password>' -e 'SQL_PASSWORD=<password>' -e 'SQL_DATABASE=solusoft-sidecar-testing' -e 'TARGET_VERSION=2.1.4' -e 'DEFAULT_SCHEMA_NAME=SIP' -e 'SCHEMA_NAME=SIP' -e 'VALIDATE_SQL_CERT=false' -v ${PWD}\src\scripts:/src/scripts -v ${PWD}\src\backups:/src/backups sql-sidecar:4.0.0

# La tabla de versiones pasa a tener este aspecto
CD_VERSION  FC_VERSION               NP_PRIM  NM_SEC  NM_REV  DS_SCHEMA
1	          2022-03-14 18:33:21.910	 0	      0	      0	      SIP
1	          2022-03-14 18:33:52.217	 4	      0	      0	      STON
2	          2022-03-14 18:33:32.153	 2	      0	      0	      SIP
3	          2022-03-14 18:33:34.263	 2	      1	      0	      SIP
4	          2022-03-14 18:33:35.183	 2	      1	      1	      SIP
5	          2022-03-14 18:33:35.813	 2	      1	      2	      SIP
6	          2022-03-14 18:33:37.873	 2	      1	      3	      SIP
7	          2022-03-14 18:34:07.613	 2	      1	      4	      SIP

# A partir de ahora se pueden versionar en la base de datos más esquemas.
docker run --rm --name sql-sidecar -e 'START_SQL_SERVER=false' -e 'CREATE_DATABASE=false' -e 'SQL_SERVER=solusoft-sidecar-testing-server.database.windows.net' -e 'SQL_USER=soluuser' -e 'SA_PASSWORD=<password>' -e 'SQL_PASSWORD=<password>' -e 'SQL_DATABASE=solusoft-sidecar-testing' -e 'TARGET_VERSION=1.0.0' -e 'SCHEMA_NAME=ACL' -e 'VALIDATE_SQL_CERT=false' -v ${PWD}\src\scriptsACL:/src/scripts -v ${PWD}\src\backups:/src/backups sql-sidecar:4.0.0

# La tabla de versiones queda así
CD_VERSION  FC_VERSION               NP_PRIM  NM_SEC  NM_REV  DS_SCHEMA
1	          2022-03-14 18:38:26.700	 1	      0	      0	      ACL
1	          2022-03-14 18:33:21.910	 0	      0	      0	      SIP
1	          2022-03-14 18:33:52.217	 4	      0	      0	      STON
2	          2022-03-14 18:33:32.153	 2	      0	      0	      SIP
3	          2022-03-14 18:33:34.263	 2	      1	      0	      SIP
4	          2022-03-14 18:33:35.183	 2	      1	      1	      SIP
5	          2022-03-14 18:33:35.813	 2	      1	      2	      SIP
6	          2022-03-14 18:33:37.873	 2	      1	      3	      SIP
7	          2022-03-14 18:34:07.613	 2	      1	      4	      SIP
```

## Contributing & License

solusoft  

Todos los derechos reservados. Consultar [www.solusoft.es](http://www.solusoft.es/contactar.aspx)  