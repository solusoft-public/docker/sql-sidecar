// requires
const express = require('express');
const { createTerminus } = require('@godaddy/terminus');
const bodyParser = require('body-parser');
const VersionManager = require("./bll/VersionManager");
const SQLHelper = require('./bll/SQLHelper');
const { spawn } = require("child_process");
const FileManager = require('./bll/FileManager');

// constants
const SQLSERVER_COMMAND = "/opt/mssql/bin/sqlservr";
const STARTUP_WAIT_TIME = 30 * 1000; // 30 seconds


async function getConfig() {
      
    async function getLastVersionFolder(scriptsFolder) {
        var lastVersionFolder = null;
        try {                    
            const subfolders = await FileManager.readSubFolders(scriptsFolder);            
            lastVersionFolder = subfolders[subfolders.length - 1];
            // get the folder name
            lastVersionFolder = lastVersionFolder.split("/").pop();
            console.log("Last version folder found was: [" + lastVersionFolder + "]");
        } catch(err) {
            console.log("Last version folder couldn't be obtained: " + err);
        }
        return lastVersionFolder;
    }
    // Load Environment variables
    const {
        START_SQL_SERVER,
        CREATE_DATABASE,
        SA_PASSWORD,
        SQL_SERVER,
        SQL_USER,
        SQL_PASSWORD,
        SQL_DATABASE,
        TARGET_VERSION,
        SCRIPTS_FOLDER,
        ENABLE_BACKUPS,
        BACKUPS_FOLDER,
        SQLCMD_PATH,
        VALIDATE_SQL_CERT,
        API_PORT,
        PREFIX_NAME,
        SCHEMA_NAME,
    } = process.env;
    const defaultTargetVersion = await getLastVersionFolder(SCRIPTS_FOLDER);
    const startSQLServer = START_SQL_SERVER === "true";
    const validateSQLCert = VALIDATE_SQL_CERT === "true";
    const enableBackups = ENABLE_BACKUPS === "true";
    const createDatabase = CREATE_DATABASE === "true";
    const targetVersion = TARGET_VERSION ? TARGET_VERSION : defaultTargetVersion;
    console.log("TARGET VERSION: [" + TARGET_VERSION + "] DEFAULT WAS: [" + defaultTargetVersion + "] target... -> [" + targetVersion + "]");
    return {
        sqlServer: SQL_SERVER,
        sqlUser: SQL_USER,
        saPassword: SA_PASSWORD,
        sqlPassword: SQL_PASSWORD,
        sqlDatabase: SQL_DATABASE,
        scriptsFolder: SCRIPTS_FOLDER,
        enableBackups,
        backupsFolder: BACKUPS_FOLDER,
        sqlCmdPath: SQLCMD_PATH,
        validateSQLCert,
        prefixName: PREFIX_NAME,
        schemaName: SCHEMA_NAME,
        startSQLServer,
        createDatabase,
        targetVersion,
        apiPort: API_PORT,
    };
}


function startAPI(sqlHelper, apiPort, target_version) {
    //instanciacion
    const app = express();
    //configuración
    app.use(bodyParser.json());
    const port = apiPort || 80;
    const server = app.listen(port, err => {
        if (err) console.error("Error listeing to port", err);
        else console.log({ message: 'Server ready on port ' + server.address().port, domain: "sql-sidecar" });
    });
    //healthchecks
    async function readyCheck(sqlHelper) {
        const updatedVersion = sqlHelper.getUpdatedVersion();
        const compare = VersionManager.compareVersions(target_version, updatedVersion);
        const updated = compare === 0;

        const proceduresCheckStatus = await sqlHelper.checkProceduresAndFunctions();

        let errorMessage = null;
        if(updated && proceduresCheckStatus.isOk) errorMessage = null;
        else if (!updated) errorMessage = "Update process did not finish";
        else if (!proceduresCheckStatus.isOk) errorMessage = "Some procedures or functions could be incorrect";
        else errorMessage = "Unknown cause, but the service is not ready.";

        console.log("startApi message: " + errorMessage, { updated, target_version, updatedVersion, proceduresCheckStatus });

        let result;
        if (errorMessage) result = Promise.reject(errorMessage);
        else result = Promise.resolve({ updated, target_version, updatedVersion, proceduresCheckStatus});

        return result;
    }
    function liveCheck(server) {
        let listening = server.listening;
        if(listening) result = Promise.resolve();
        else result = Promise.reject("Server is not listening");
        return result;
    }
    const terminusOptions = {
        // health check options
        healthChecks: {
            '/health/ready': () => readyCheck(sqlHelper),    // a function returning a promise indicating service health,
            '/health/live': () => liveCheck(server),    // a function returning a promise indicating service health,
        },
        verbatim: true //devuelve el objeto que le pasemos
    };
    createTerminus(server, terminusOptions);
}

async function main() {
    const {
        sqlServer,
        sqlUser,
        saPassword,
        sqlPassword,
        sqlDatabase,
        scriptsFolder,
        enableBackups,
        backupsFolder,
        sqlCmdPath,
        validateSQLCert,
        prefixName,
        schemaName,
        startSQLServer,
        createDatabase,
        targetVersion,
        apiPort,
    } = await getConfig();
    if (!VersionManager.validateSemver(targetVersion)) {
        const error = `Target version [${targetVersion}] must be in semver format`;
        console.warn(error);
        process.exit(-1);
    } else {
        let sqlHelper;
        try {
            // SQL Helper
            sqlHelper = new SQLHelper({
              sqlServer,
              sqlUser,
              sqlPassword,
              sqlDatabase,
              scriptsFolder,
              enableBackups,
              backupsFolder,
              sqlCmdPath,
              validateSQLCert,
              prefixName,
              schemaName,
            });
            // API start
            if(startSQLServer) startAPI(sqlHelper, apiPort, targetVersion);
            // database creation
            if (createDatabase) await SQLHelper.createDatabase({ sqlServer, saPassword, sqlDatabase });
            // execute update
            const updatedVersion = await sqlHelper.doUpdate({ targetVersion });
            // checking update result
            const compare = VersionManager.compareVersions(targetVersion, updatedVersion);
            if (compare !== 0) console.warn(`version mismatch, target version [${targetVersion}], current version [${updatedVersion}]`);
        } catch (err) {
            console.error("Error doing update", err);
            process.exit(-1);
        }
    }
}

// SQLCMDPASSWORD variable
const startSQLServer = process.env.START_SQL_SERVER === "true";
process.env.SQLCMDPASSWORD = process.env.SQL_PASSWORD;

// if START SQL SERVER is true we wait some time
if(startSQLServer) {
    console.log("starting SQL server");
    const mssqlProcess = spawn(SQLSERVER_COMMAND, [], { detached: true });
    mssqlProcess.stderr.on("data", data => {
        console.error(`mssqlProcess stderr: ${data}`);
    });
    mssqlProcess.on('error', (error) => {
        console.error(`mssqlProcess error: ${error.message}`);
    });
    setTimeout(main, STARTUP_WAIT_TIME);
}
else main();