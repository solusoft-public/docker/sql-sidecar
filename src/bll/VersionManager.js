const semver = require('semver');

class VersionManager {

    /**
     * Validates if a version name is semver compliant
     * @param {string} versionName Version in semver format
     */
    static validateSemver(versionName) {        
        return semver.valid(versionName) != null;;
    }

    /**
     * Compares two semvers
     * @param {string} version1 First Sem Ver
     * @param {string} version2 Second Sem Ver
     * @return {number} 0 if equals, 1 if version1 > version2, 2 otherwise
     */
    static compareVersions(version1, version2) {
        let compare = 0;
        //if not are equals we compare, then result is 0
        if(version1 !== version2) compare = semver.gt(version1, version2) ? 1 : -1;
        return compare;
    }
}

module.exports = VersionManager;