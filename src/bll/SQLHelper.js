const sql = require("mssql");
const path = require('path');
const promiseRetry = require('promise-retry');
const FileManager = require("./FileManager");
const VersionManager = require("./VersionManager");
const sqlcmd = require('./sqlcmd-runner');
const config = require("../configuration/config.json");
const url = require("url");

//constants
const TARGET_SCRIPTS_VERSION = "4.0.0";

//symbols
const _sqlServer = Symbol();
const _sqlUser = Symbol();
const _sqlPassword = Symbol();
const _sqlDatabase = Symbol();
const _scriptsFolder = Symbol();
const _enableBackups = Symbol();
const _backupsFolder = Symbol();
const _sqlCmdPath = Symbol();
const _validateSQLCert = Symbol();
const _pool = Symbol();
const _updatedVersion = Symbol();
const _prefixName = Symbol();
const _schemaName = Symbol();

//methods
const _connect = Symbol();
const _close = Symbol();
const _installSupportScripts = Symbol();
const _getCurrentVersion = Symbol();
const _updateLockStatus  = Symbol();
const _updateDatabaseVersion = Symbol();
const _acquireLock = Symbol();
const _executeSQLScripts = Symbol();
const _executeSQLQuery  = Symbol();
const _doBackup = Symbol();
const _doUpdateVersion = Symbol();


//override connect with retries and parameters
function connectRetries({ configRetries, user, password, server, database }) {
  return promiseRetry(configRetries, async (retry, number) => {
    let pool = null;
    try {
      console.log(`connecting to database [${database}], try number [${number}]`);
      // codificamos en url usuario y la password para que no haya problemas con caracteres especiales
      user = encodeURIComponent(user);
      password = encodeURIComponent(password);
      // creamos la url de conexión
      const connectUrl = url.parse(`mssql://${user}:${password}@${server}/${database}?encrypt=true`);
      pool = await sql.connect(connectUrl.href);
      if (!pool.connected) {
        await pool.close();
        pool = null;
        throw new Error("not connected");
      }
      return pool;
    } catch (err) {
      console.error("there was an error connecting to database", err.message);
      retry(err);
    }
  });
}


class SQLHelper {

  constructor({ sqlServer, sqlUser, sqlPassword, sqlDatabase, scriptsFolder, enableBackups, backupsFolder, sqlCmdPath, validateSQLCert, prefixName, schemaName, }) {
    this[_sqlServer] = sqlServer;
    this[_sqlUser] = sqlUser;
    this[_sqlPassword] = sqlPassword;
    this[_sqlDatabase] = sqlDatabase;
    this[_scriptsFolder] = scriptsFolder;
    this[_enableBackups] = enableBackups;
    this[_backupsFolder] = backupsFolder;
    this[_sqlCmdPath] = sqlCmdPath;
    this[_validateSQLCert] = validateSQLCert;
    this[_prefixName] = prefixName;
    this[_schemaName] = schemaName;
    this[_pool] = null;
    this[_updatedVersion] = null;
  }

  /**
   * Connection with T-SQL DB for executing stored procedures and queries
   */
  async [_connect]() {
    const { connection } = config;
    if (this[_pool] == null) this[_pool] = await connectRetries({ configRetries: connection, user: this[_sqlUser], password: this[_sqlPassword], server: this[_sqlServer], database: this[_sqlDatabase] });
    return this[_pool];
  }

  /**
   * Close connection
   */
  async [_close]() {
    //closing connection
    if (this[_pool] != null) {
      await this[_pool].close();
      this[_pool] = null;
    }
  }

  getUpdatedVersion() {
    return this[_updatedVersion];
  }

  /**
   * create the target database
   */
  static async createDatabase({ sqlServer, saPassword, sqlDatabase }) {
    let pool;
    try {
      //we need to connect to master database
      const { connection } = config;
      pool = await connectRetries({ configRetries: connection, user: "sa", password: saPassword, server: sqlServer, database: "master" });
      console.log(`creating database [${sqlDatabase}]`);
      await pool.request().query(`CREATE DATABASE "${sqlDatabase}"`);
      console.log(`database created`);
      if (pool) await pool.close();
    } catch (err) {
      console.warn(`WARNING: possible error creating database [${sqlDatabase}]`, err.message);
      if (pool) await pool.close();
    }
  }

  /**
   * Install support scripts in database
   */
  async [_installSupportScripts]() {
    try {
      const schema = this[_prefixName];
      const installFolder = path.join(__dirname, "../support/install/");
      const defaultVersion = await this[_getCurrentVersion]({ schema, });
      const targetVersion = TARGET_SCRIPTS_VERSION;
      await this[_doUpdateVersion]({ schema, defaultVersion, targetVersion, scriptsFolder: installFolder, enableBackups: false, });

    } catch (err) { 
      console.error("error instaling support scripts", err);
    }
  }

  /**
   * Get the current version (the version with the latest date) for a SQL Database
   */
  async [_getCurrentVersion]({ schema = this[_schemaName], defaultVersion = "0.0.0" } = {}) {
    let version = defaultVersion;
    try {
      const { recordset: versions } = await this[_pool].request()
        .input("DS_SCHEMA", sql.VarChar(255), schema)
        .execute(`${this[_prefixName]}_C_VERSION_SEL_LATEST`);
      const { DS_VERSION } = versions[0] || {};
      if (DS_VERSION)
        version = DS_VERSION;
    } catch (err) {
      console.warn("error getting current version", schema, defaultVersion);
    }
    return version;
  }

  /**
   * Updates the database to a version
   * @param {string} versionName Desired version
   */
  [_updateDatabaseVersion]({ schema = this[_schemaName], versionName } = {}) {
    const [nmPrim, nmSec, nmRev] = versionName.split(".");
    console.log(`Updating database schema version to [${nmPrim}.${nmSec}.${nmRev}]...`);
    return this[_pool].request()
      .input("DS_SCHEMA", sql.VarChar(255), schema)
      .input("NM_PRIM", sql.SmallInt, nmPrim)
      .input("NM_SEC", sql.SmallInt, nmSec)
      .input("NM_REV", sql.SmallInt, nmRev)
      .execute(`${this[_prefixName]}_C_VERSION_INSERTUPDATE`);
  }

  /**
   * Set lock status
   * @param {Boolean} lock lock status
   */
  async [_updateLockStatus](lock) {    
    console.log(`Setting lock status to [${lock}] for database [${this[_sqlDatabase]}]...`);
    const { recordset: locks } = await this[_pool].request()
      .input("IT_LOCK", sql.Bit, lock)
      .execute(`${this[_prefixName]}_C_LOCK_UPDATE_LOCK`);
    const { OLD_IT_LOCK, NEW_IT_LOCK } = locks[0] || {};
    const oldLockStatus = OLD_IT_LOCK === true;
    const newLockStatus = NEW_IT_LOCK === true;
    return { newLockStatus, oldLockStatus };    
  }

  /**
   * Tries to acquire the lock, waiting if it was acquired
   * @param {number} sleepTime sleep time for waiting for lock
   */
  async [_acquireLock](sleepTime = 10000) {    
    //sleep function
    const sleep = (ms) => new Promise(r => setTimeout(r, ms));
    //setting lock to true and getting previous lock status
    let { oldLockStatus, newLockStatus } = await this[_updateLockStatus](true);
    //we wait when the lock does not change its value except for the case when the lock does not change because timeout (old and new lock status will be true)
    while(oldLockStatus === newLockStatus && oldLockStatus === false) {
      console.log(`database is locked, waiting...`);      
      await sleep(sleepTime);
      //check new value
      const { oldLockStatus: _old, newLockStatus: _new } = await this[_updateLockStatus](true);
      oldLockStatus = _old;
      newLockStatus = _new;
    }
  }

  /**
   * Executes all SQL scripts contained on a folder
   * @param {string} folder Container folder
   */
  async [_executeSQLScripts](folder, failOnSqlErrors = true) {
    const sqlFiles = await FileManager.readSQLFiles(folder);
    const sqlcmdConfig = {
      commandPath: this[_sqlCmdPath],
      server: {
        name: this[_sqlServer]
      },
      username: this[_sqlUser],
      database: this[_sqlDatabase],
      inputFiles: sqlFiles,
      encryptedConnection: true,
      trustServerCert: !this[_validateSQLCert],
      failOnSqlErrors,
      errorLevel: -1,
    };
    return sqlcmd(sqlcmdConfig);
  }

  /**
   * Executes all SQL query
   * @param {string} folder Container folder
   */
  [_executeSQLQuery](query, failOnSqlErrors = true) {    
    const sqlcmdConfig = {
      commandPath: this[_sqlCmdPath],
      server: {
        name: this[_sqlServer]
      },
      username: this[_sqlUser],
      database: this[_sqlDatabase],
      query,
      encryptedConnection: true,
      trustServerCert: !this[_validateSQLCert],
      failOnSqlErrors,
      errorLevel: -1,
    };
    return sqlcmd(sqlcmdConfig);
  }

  /**
   * Executes backup command for the current version
   * @param {string} currentVersionName version name
   */
  async [_doBackup](currentVersionName) {
    try {
      //crete backup folder
      const backupFolder = path.join(this[_backupsFolder], currentVersionName);
      FileManager.createFolder(backupFolder);
      //doing standard backup
      const backupFileName = `${this[_sqlDatabase]}.bak`;
      const backupName = `${this[_sqlDatabase]}-full`;
      const destPath = path.join(backupFolder, backupFileName);
      const standardBackupQuery = `BACKUP DATABASE [${this[_sqlDatabase]}] TO DISK = N'${destPath}' WITH NOFORMAT, NOINIT, NAME = '${backupName}', SKIP, NOREWIND, NOUNLOAD, STATS = 10`;
      await this[_executeSQLQuery](standardBackupQuery);
      //doing log backup
      const backupLogFileName = `${this[_sqlDatabase]}_LogBackup.bak`;
      const backupLogName = `${this[_sqlDatabase]}_LogBackup`;
      const destBackupLogPath = path.join(backupFolder, backupLogFileName);
      const backupLogQuery = `BACKUP LOG [${this[_sqlDatabase]}] TO DISK = N'${destBackupLogPath}' WITH NOFORMAT, NOINIT, NAME = N'${backupLogName}', NOSKIP, NOREWIND, NOUNLOAD, STATS = 5`;
      await this[_executeSQLQuery](backupLogQuery);
    } catch (err) {
      console.warn(`WARNING: possible error creating backups for version [${currentVersionName}]`, err.message);
    }
  }

  /*
    Comprueba que los procedimientos y funciones compilan y que su codigo parece valido.
    Devuelve un objeto que indica si todo va bien, o false en otro caso.
    { isOk: bool, data: object }
  */
  async checkProceduresAndFunctions() {
    let status = {
      isOk: false,
      data: null
    };
    
    try {
      //connection with database
      await this[_connect]();

      //consultamos el check de procedimientos
      //Si el resultado es superior a 0, es un error.
      let result = await this[_pool].request().execute(`${this[_prefixName]}_SUPPORT_pr_HEALTHCHECK`);
      status.isOk = result.returnValue == 0;
      status.data = result;

      console.log("checkProceduresAndFunctions result " + status.isOk, status);

      await this[_close]();
    } catch (err) {
      console.error("error during checkProceduresAndFunctions", err);
      //close connection
      status.isOk = false;
      status.data = err;
      await this[_close]();
      throw err;
    } 
    
    return status;
  }

  async doUpdate({ schema = this[_schemaName], targetVersion, scriptsFolder = this[_scriptsFolder], enableBackups = this[_enableBackups], backupsFolder = this[_backupsFolder] }) {
    //shutdown function
    const shutdown = async () => {
      try {
        console.log(`executing shutdown process`);
        //setting lock to false
        if(this[_pool]) await this[_updateLockStatus](false);
        //close connection
        await this[_close]();
        process.exit(-1);
      } catch (err) {
        console.error("error during shutdown", err);
        throw err;
      }
    };
    let updatedVersion;
    try {
      //connection with database
      await this[_connect]();
      //installing support scripts
      await this[_installSupportScripts]();
      //waiting for unlock status
      await this[_acquireLock]();
      //capturing signals for graceful shutdown
      process.on('SIGTERM', shutdown);
      process.on('SIGINT', shutdown);
      updatedVersion = await this[_doUpdateVersion]({ schema, targetVersion, scriptsFolder, enableBackups, backupsFolder });
      //setting lock to false
      await this[_updateLockStatus](false);
      //close connection
      await this[_close]();
    } catch (err) {
      //setting lock to false
      await this[_updateLockStatus](false);
      //close connection
      await this[_close]();
      throw err;
    } 
    this[_updatedVersion] = updatedVersion;
    return updatedVersion;
  }

  async [_doUpdateVersion]({ schema, defaultVersion = "0.0.0", targetVersion, scriptsFolder, enableBackups, backupsFolder, }) {    
    let _currentVersion = await this[_getCurrentVersion]({ schema, defaultVersion });
    console.log(`Starting update process for Database [${this[_sqlDatabase]}] and schema [${schema}] in Server [${this[_sqlServer]}]...`);
    console.log(`Database schema is currently in version [${_currentVersion}]`);
    //getting current version for database    
    const cTargetCurrent = VersionManager.compareVersions(targetVersion, _currentVersion);
    if (cTargetCurrent <= 0) {
      //update ignored
      console.warn(`Target version [${targetVersion}] is lower or equal to the current version [${_currentVersion}] of the database. The update process will be omited`);
    } 
    else {
      //doing update
      console.log(`Updating Database schema to version [${targetVersion}]...`);
      console.log(`Update scripts in folder [${scriptsFolder}]`);
      console.log(`Backups in folder [${backupsFolder}]`);
      //iterating script folders
      const folders = await FileManager.readSubFolders(scriptsFolder);
      for (const scriptsVersionFolder of folders) {
        //we keep only folder name          
        const scriptsVersion = path.basename(scriptsVersionFolder);
        //if scripts version is in between current and target...
        const cScriptsCurrent = VersionManager.compareVersions(scriptsVersion, _currentVersion);
        const cScriptsTarget = VersionManager.compareVersions(scriptsVersion, targetVersion);
        if (cScriptsCurrent == 1 && cScriptsTarget <= 0) {
          //we backup the current database version
          if(enableBackups && _currentVersion !== "0.0.0") await this[_doBackup](_currentVersion);
          console.log(`Executing scripts for version [${scriptsVersion}]...`);
          //then we execute the scripts and update the database version
          await this[_executeSQLScripts](scriptsVersionFolder);
          await this[_updateDatabaseVersion]({ schema, versionName: scriptsVersion });
          _currentVersion = scriptsVersion;
        }
      }
      console.log(`Update process ended`);
    }
    //set updatedVersion
    const updatedVersion = await this[_getCurrentVersion]({ schema });
    return updatedVersion;
  }
}

module.exports = SQLHelper;