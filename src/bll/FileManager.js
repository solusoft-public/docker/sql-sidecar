const fs = require('fs').promises;
const fsSync = require('fs');
const path = require('path');
const mkpath = require('mkpath');
const VersionManager = require('./VersionManager');

class FileManager {

    /**
     * Get, in order, the subfolders inside a folder
     * @param {string} folder Desired folder
     */
    static async readSubFolders(folder) {
      //check if a folder is a directory
        const isDirectory = source => fsSync.lstatSync(source).isDirectory();
        //check if a folder name is semver compatible
        const validFolderName = folder => {
            const folderName = path.basename(folder);
            const valid = VersionManager.validateSemver(folderName);
            if(!valid) console.warn(`WARNING: folder name [${folderName}] is not in semver format, ignored`);
            return valid;
        }
        //read all files in a directory
        const readFiles = source => fs.readdir(source);
        //filter files in a directory for keeping only directories
        const filterFolders = files =>
            files                
                .map(name => path.join(folder, name))
                .filter(isDirectory)
                .filter(validFolderName);
        //compare two folder names skipping all characters unless numbers
        const compareFolders = (folderA, folderB) => {
            const versionA = path.basename(folderA);
            const versionB = path.basename(folderB);
            return VersionManager.compareVersions(versionA, versionB);
        };
        const files = await readFiles(folder);
        const folders = filterFolders(files);
        const sortedFolders = folders.sort(compareFolders);
        return sortedFolders;
    }

    /**
     * Get, in order, the SQL files inside a folder
     * @param {string} folder Desired folder
     */
    static async readSQLFiles(folder) {
        //check if a file has the .sql extension
        const isSQLFile = source => path.extname(source) == ".sql";
         //check if a folder name is semver compatible
         const validSQLFilename = file => {
            const filename = path.basename(file);
            const regex = /^\d+\./;
            const m = regex.exec(filename);
            const valid = m != null && m.length === 1;
            if(!valid) console.warn(`WARNING: SQL file name [${filename}] is not valid, it must start with number followed by a dot. It will be ignored`);
            return valid;
        }
        //read all files in a folder
        const readFiles = source => fs.readdir(source);
        //filter all files in a folder for keeping only files with .sql extension
        const filterSQLFiles = files =>
            files
                .map(name => path.join(folder, name))                
                .filter(isSQLFile)
                .filter(validSQLFilename);
        //compare two file names using their prefixes
        const compareSQLFiles = (fileA, fileB) => {
            const orderFileA = path.basename(fileA).split(".")[0];
            const orderFileB = path.basename(fileB).split(".")[0];
            const orderA = Number(orderFileA);
            const orderB = Number(orderFileB);
            return orderA - orderB;
        };
        const files = await readFiles(folder);
        const sqlFiles = filterSQLFiles(files);
        const sorted = sqlFiles.sort(compareSQLFiles);
        return sorted;
    }

    /**
     * Create a folder with all subpath
     * @param {string} folder Folder path
     */
    static createFolder(folder) {
        mkpath.sync(folder);
    }


}

module.exports = FileManager;