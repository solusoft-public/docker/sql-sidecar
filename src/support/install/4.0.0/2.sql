GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:on error exit
GO

/*
Detectar el modo SQLCMD y deshabilitar la ejecución del script si no se admite el modo SQLCMD.
Para volver a habilitar el script después de habilitar el modo SQLCMD, ejecute lo siguiente:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'El modo SQLCMD debe estar habilitado para ejecutar correctamente este script.';
        SET NOEXEC ON;
    END


GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_SCHEMA = 'dbo' AND ROUTINE_NAME = '$(PREFIX_NAME)_SUPPORT_pr_HEALTHCHECK')
    DROP PROCEDURE [dbo].[$(PREFIX_NAME)_SUPPORT_pr_HEALTHCHECK]
GO

/*
OBJECT: Hace un refreshsqlmodule de todos los procedimeintos y funciones para chequear su coherencia

USE: STON

Autor    Fecha         Revisión
----------------------------------
DCM		06/07/2021		Create
*/

CREATE PROCEDURE [dbo].[$(PREFIX_NAME)_SUPPORT_pr_HEALTHCHECK]
	 -- Parámetros de entrada (opcionales)
	@UserName nvarchar(256) = null
	, @ApplicationName nvarchar(256) = null
AS

	-- Bloque de declaración de variables
	DECLARE @CodigoError int
	DECLARE @MensajeError varchar(1000)
	DECLARE @ERROR_NUMBER int
	DECLARE @ERROR_PROCEDURE varchar(256)
	DECLARE @ERROR_MESSAGE varchar(256)
	DECLARE @Resultado int

	-- Bloque de declaración de constantes.
		-- Ninguna.
	
BEGIN
	
    -- Asignación de variables.
    SET @CodigoError = 0
	
    -- Validaciones.
		-- Ninguna.
	
    BEGIN TRY
				
		DECLARE @$(PREFIX_NAME)_TEMP_TABLE_OBJECTS TABLE (CD_NM_OBJETO int, DS_NOMBRE_OBJETO varchar(100), DS_QUERY varchar(300))

		---- Opcion 01 Solo procedimientos
		--INSERT INTO @$(PREFIX_NAME)_TEMP_TABLE_OBJECTS
		--	(CD_NM_OBJETO, DS_NOMBRE_OBJETO, DS_QUERY)
		--SELECT  CD_NM_OBJETO = ROW_NUMBER() OVER (ORDER BY name)
		--	, DS_NOMBRE_OBJETO = p.[name]
		--	, 'Print ''' + p.[name] + '''

		--EXECUTE sp_refreshsqlmodule ''' + p.[name] + '''

		--GO

		--'
		--FROM sys.procedures p
		--ORDER BY p.[name]

		---- Opcion 02 Procedimientos y funciones
		INSERT INTO @$(PREFIX_NAME)_TEMP_TABLE_OBJECTS
			(CD_NM_OBJETO, DS_NOMBRE_OBJETO, DS_QUERY)
		SELECT  CD_NM_OBJETO = ROW_NUMBER() OVER (ORDER BY r.[ROUTINE_NAME])
			, DS_NOMBRE_OBJETO = r.[ROUTINE_NAME]
			, 'Print ''' + r.[ROUTINE_NAME] + '''

				EXECUTE sp_refreshsqlmodule ''' + r.[ROUTINE_NAME] + '''

				GO

		'
		FROM INFORMATION_SCHEMA.ROUTINES r
		ORDER BY r.[ROUTINE_NAME]

		DECLARE @NM_CONT int = 1 
		DECLARE @NM_MAX int = (SELECT TOP 1 MAX(CD_NM_OBJETO) FROM @$(PREFIX_NAME)_TEMP_TABLE_OBJECTS)

		WHILE (@NM_CONT <= @NM_MAX)
		BEGIN
			DECLARE @NM_OBJETO varchar(100)
			DECLARE @DS_OBJETO varchar(100)
			DECLARE @DS_QUERY varchar(100)

			SELECT @NM_OBJETO = CD_NM_OBJETO, @DS_OBJETO = DS_NOMBRE_OBJETO, @DS_QUERY = DS_QUERY FROM @$(PREFIX_NAME)_TEMP_TABLE_OBJECTS WHERE CD_NM_OBJETO = @NM_CONT
	
			PRINT CONCAT(@NM_OBJETO, '. ', @DS_OBJETO)

			---- Ejecucion Manual
			--PRINT CONCAT('EXECUTE sp_refreshsqlmodule ', @DS_OBJETO)
			--PRINT 'GO'
	
			-- Ejecucion Automatica
			EXEC sp_refreshsqlmodule @DS_OBJETO
			PRINT 'GO'

			SET @NM_CONT = @NM_CONT + 1
		END
		
		RETURN @CodigoError
    END TRY
	
    BEGIN CATCH
		SET @CodigoError = 10000	-- Codigo de error por defecto. Se ha producido un error general en el sistema

		--SET @ERROR_NUMBER = ERROR_NUMBER()
  --      SET @ERROR_PROCEDURE = ERROR_PROCEDURE()
  --      SET @ERROR_MESSAGE = ERROR_MESSAGE()
        
  --      IF @ERROR_NUMBER = 547 -- Se referencia desde otras tablas
		--	SET @CodigoError = 10002
		--ELSE IF (@ERROR_NUMBER = 2627 ) -- Clave primaria duplicada
		--	SET @CodigoError = 10001

		--SET @MensajeError = convert(varchar,IsNull(@CodigoError,-1)) + ' - ' + IsNull(@ERROR_PROCEDURE,'') + ': ' + 
		--	convert(varchar,IsNull(@ERROR_NUMBER,-1)) + ' - ' + IsNull(@ERROR_MESSAGE,'')
			
  --      -- Insertamos el error detallado
		--EXEC @Resultado = STON_pr_LOGS_LOG_Ins @USUARIO = @UserName,  @ACCION = @MensajeError, @SISTEMA = 1, @TIPOREG = 1, @APLICACION = @ApplicationName

  --      IF @Resultado > 0
		--	SET @CodigoError = 10003

        RETURN @CodigoError
    END CATCH
END