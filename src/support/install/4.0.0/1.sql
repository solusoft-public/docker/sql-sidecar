GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:on error exit
GO

/*
Detectar el modo SQLCMD y deshabilitar la ejecución del script si no se admite el modo SQLCMD.
Para volver a habilitar el script después de habilitar el modo SQLCMD, ejecute lo siguiente:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'El modo SQLCMD debe estar habilitado para ejecutar correctamente este script.';
        SET NOEXEC ON;
    END


GO


-- ==============================
--           VERSIONS
-- ==============================
IF NOT EXISTS (SELECT 1 FROM sys.tables WHERE name  = '$(PREFIX_NAME)_C_VERSION')
    BEGIN    
        CREATE TABLE [dbo].[$(PREFIX_NAME)_C_VERSION](
            [CD_VERSION] [int] NOT NULL,
            [DS_SCHEMA] nvarchar(255) NOT NULL,
            [FC_VERSION] [datetime] NOT NULL,
            [NM_PRIM] [smallint] NULL,
            [NM_SEC] [smallint] NULL,
            [NM_REV] [smallint] NULL
        )    
        ALTER TABLE [dbo].[$(PREFIX_NAME)_C_VERSION] ADD  CONSTRAINT [PK_$(PREFIX_NAME)_C_VERSION] PRIMARY KEY CLUSTERED 
        (
            [CD_VERSION], [DS_SCHEMA] ASC
        ) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]    
        ALTER TABLE [dbo].[$(PREFIX_NAME)_C_VERSION] ADD  CONSTRAINT [DF_$(PREFIX_NAME)_C_VERSION_FC_VERSION]  DEFAULT (getdate()) FOR [FC_VERSION]
    END
ELSE
    BEGIN
        ALTER TABLE [dbo].[$(PREFIX_NAME)_C_VERSION]
        ADD DS_SCHEMA nvarchar(255) NOT NULL CONSTRAINT [DEFAULT_SCHEMA] DEFAULT '$(DEFAULT_SCHEMA_NAME)' WITH VALUES
        ALTER TABLE [dbo].[$(PREFIX_NAME)_C_VERSION]
        DROP CONSTRAINT [PK_$(PREFIX_NAME)_C_VERSION]
        ALTER TABLE [dbo].[$(PREFIX_NAME)_C_VERSION] ADD  CONSTRAINT [PK_$(PREFIX_NAME)_C_VERSION] PRIMARY KEY CLUSTERED 
        (
            [CD_VERSION], [DS_SCHEMA] ASC
        ) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)
    END
GO


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_SCHEMA = 'dbo' AND ROUTINE_NAME = '$(PREFIX_NAME)_C_VERSION_INSERTUPDATE')
    DROP PROCEDURE [dbo].[$(PREFIX_NAME)_C_VERSION_INSERTUPDATE]
GO
-- =============================================
-- Author:		AAG
-- Create date: 05/12/2019
-- Description:	Actualiza o crea una versión de aplicación
-- =============================================
CREATE PROCEDURE [dbo].[$(PREFIX_NAME)_C_VERSION_INSERTUPDATE]
    @DS_SCHEMA nvarchar(255),
    @NM_PRIM SMALLINT,
    @NM_SEC SMALLINT,
    @NM_REV SMALLINT
AS
BEGIN	
	SET NOCOUNT ON;

    DECLARE @CD_VERSION INT = (SELECT CD_VERSION FROM [$(PREFIX_NAME)_C_VERSION] WHERE DS_SCHEMA = @DS_SCHEMA AND NM_PRIM = @NM_PRIM AND NM_SEC = @NM_SEC AND NM_REV = @NM_REV)
    IF (@CD_VERSION IS NOT NULL) 
        UPDATE [$(PREFIX_NAME)_C_VERSION] SET FC_VERSION = GETDATE() WHERE CD_VERSION = @CD_VERSION
    ELSE
        BEGIN
            DECLARE @NEW_CD_VERSION int = (SELECT ISNULL(MAX(CD_VERSION), 0) + 1 FROM [$(PREFIX_NAME)_C_VERSION] WHERE DS_SCHEMA = @DS_SCHEMA)
            INSERT INTO [$(PREFIX_NAME)_C_VERSION] (CD_VERSION, DS_SCHEMA, FC_VERSION, NM_PRIM, NM_SEC, NM_REV) 
                VALUES (@NEW_CD_VERSION, @DS_SCHEMA, GETDATE(), @NM_PRIM, @NM_SEC, @NM_REV)
        END
END
GO


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_SCHEMA = 'dbo' AND ROUTINE_NAME = '$(PREFIX_NAME)_C_VERSION_SEL_LATEST')
    DROP PROCEDURE [dbo].[$(PREFIX_NAME)_C_VERSION_SEL_LATEST]
GO
-- =============================================
-- Author:		AAG
-- Create date: 05/12/2019
-- Description:	Selecciona la última versión de aplicación
-- =============================================
CREATE PROCEDURE [dbo].[$(PREFIX_NAME)_C_VERSION_SEL_LATEST]
    @DS_SCHEMA nvarchar(255)
AS
BEGIN	
	SET NOCOUNT ON;
    
    DECLARE @LATEST_CD_VERSION int = (SELECT MAX(CD_VERSION) FROM [$(PREFIX_NAME)_C_VERSION] WHERE DS_SCHEMA = @DS_SCHEMA)  
    SELECT CONCAT(NM_PRIM, '.', NM_SEC, '.', NM_REV) AS DS_VERSION FROM [$(PREFIX_NAME)_C_VERSION] WHERE CD_VERSION = @LATEST_CD_VERSION AND DS_SCHEMA = @DS_SCHEMA
END
GO


-- ==============================
--             LOCKS
-- ==============================
IF NOT EXISTS (SELECT 1 FROM sys.tables WHERE name = '$(PREFIX_NAME)_C_LOCK')
BEGIN
    CREATE TABLE [dbo].[$(PREFIX_NAME)_C_LOCK](
        [CD_LOCK] [int] NOT NULL,
        [IT_LOCK] [bit] NOT NULL,
        [FC_LOCK] [datetime] NOT NULL,
        [DS_LOCK] nvarchar(255) NULL	
    )
    ALTER TABLE [dbo].[$(PREFIX_NAME)_C_LOCK] ADD  CONSTRAINT [PK_$(PREFIX_NAME)_C_LOCK] PRIMARY KEY CLUSTERED 
    (
        [CD_LOCK] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ALTER TABLE [dbo].[$(PREFIX_NAME)_C_LOCK] ADD  CONSTRAINT [DF_$(PREFIX_NAME)_C_LOCK_FC_LOCK]  DEFAULT (getdate()) FOR [FC_LOCK]
END
GO


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_SCHEMA = 'dbo' AND ROUTINE_NAME = '$(PREFIX_NAME)_C_LOCK_UPDATE_LOCK')
    DROP PROCEDURE [dbo].[$(PREFIX_NAME)_C_LOCK_UPDATE_LOCK]
GO
-- =============================================
-- Author:		AAG
-- Create date: 03/01/2020
-- Description:	Actualiza un lock

-- Changelog
-- 11/02/2020 : Hacemos que sea una operación atómica
-- =============================================
CREATE PROCEDURE [dbo].[$(PREFIX_NAME)_C_LOCK_UPDATE_LOCK]
	@IT_LOCK bit,
    @CD_LOCK INT = 1,
    @MAX_LOCK_TIME INT = 90
AS
BEGIN	
    -- Para prevenir que un lock se quede activo indefinidamente permitimos locks de sólo minuto y medio
    -- Se ha elegido este tiempo ya que por defecto el timeout de una request http es de 2 minutos            
    UPDATE [$(PREFIX_NAME)_C_LOCK] 
    SET
        IT_LOCK = @IT_LOCK,
        FC_LOCK = GETDATE()
    OUTPUT deleted.IT_LOCK AS OLD_IT_LOCK, inserted.IT_LOCK AS NEW_IT_LOCK
    WHERE CD_LOCK = @CD_LOCK AND (IT_LOCK <> @IT_LOCK OR DATEDIFF(second, FC_LOCK, GETDATE()) > @MAX_LOCK_TIME)
END
GO


-- ==============================
--             DATA
-- ==============================
IF NOT EXISTS (SELECT 1 FROM [dbo].[$(PREFIX_NAME)_C_LOCK] WHERE CD_LOCK = 1)
BEGIN
    INSERT INTO [$(PREFIX_NAME)_C_LOCK] (CD_LOCK, IT_LOCK, FC_LOCK, DS_LOCK) VALUES (1, 0, GETDATE(), 'MAIN LOCK')
END
GO
