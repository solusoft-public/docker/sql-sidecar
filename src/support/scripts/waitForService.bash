#!/bin/bash
target_url=$1
current_code=0
target_code=${2:-200}
n_max_attemps=${3:-60}
echo "Arguments: Service Host [$target_url], Target HTTP code [$target_code], N max attempts [$n_max_attemps]";

count=0
while [[ $current_code != $target_code ]] && [[ $count -le $n_max_attemps ]];
do
    echo "trying access healthcheck...";
    current_code=$(curl -s -o /dev/null -w ''%{http_code}'' $target_url)
    res=$(curl $target_url)
    echo "waiting for service to start, response was $current_code and $res";
    sleep 5;
    ((count++))
done

if [[ $current_code != $target_code ]]
then 
    echo "Response was $current_code, this healthcheck returned an error. Canceling... "
    exit 1; #respondemos con un error
else
    echo "Service started"
fi