FROM mcr.microsoft.com/mssql/server:2019-CU17-ubuntu-18.04

## Preparacion de la imagen
USER root
### instalación de nodejs
RUN set -uex; \
    apt-get update; \
    apt-get install -y ca-certificates curl gnupg; \
    mkdir -p /etc/apt/keyrings; \
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key \
     | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg; \
    NODE_MAJOR=16; \
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" \
     > /etc/apt/sources.list.d/nodesource.list; \
    apt-get update; \
    apt-get install nodejs -y;
RUN apt-get install -y git wget nano python make nodejs iputils-ping

## Enable SSH
RUN apt-get install -y openssh-server
RUN mkdir /var/run/sshd
RUN chmod 0755 /var/run/sshd
RUN echo "root:Docker!" | chpasswd 
COPY src/sshd_config /etc/ssh/
EXPOSE 2222

## Entorno (no modificar)
ENV API_PORT="80" \
    ACCEPT_EULA='Y' \    
    SQLCMD_PATH="/opt/mssql-tools/bin/sqlcmd" \
    SCRIPTS_FOLDER="/src/scripts/" \
    ENABLE_BACKUPS="false" \
    BACKUPS_FOLDER="/src/backups/" \
    DEFAULT_SCHEMA_NAME="Main" \
    SCHEMA_NAME="Main" \
    PREFIX_NAME="STON"

## COPY sourcecode
WORKDIR /src
COPY src/package.json /src/
RUN npm install
COPY src /src

## public executable files
RUN chmod +rwx /src/support/scripts/*

# Define mountable directories.
VOLUME ["/src/scripts"]
VOLUME ["/src/backups"]

EXPOSE 1433
EXPOSE $API_PORT


CMD [ "node", "init.js" ]